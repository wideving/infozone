using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Services;
using Domain;
using MediatR;
using Persistance;

namespace Application.Transfers
{
    public class ToExternalTransfer
    {
        public static string Clearing = "5555"; //Would probably retrieve this from some sort of config file in a real project
        public class Command : IRequest
        {
            [Required]
            public string Clearing { get; set; }
            [Required]
            public string SenderAccount { get; set; }
            [Required]
            public string ReceiverAccount { get; set; }
            [Range(1, 10000000)]
            public int Amount { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DatabaseContext _context;
            private readonly IExternalTransferService _service;

            public Handler(DatabaseContext context, IExternalTransferService service)
            {
                _service = service;
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var sender = _context.Accounts.FirstOrDefault(a => a.Number == request.SenderAccount);

                if (sender == null)
                {
                    throw new ArgumentException("Det finns ingen avsändare med det kontonumret");
                }

                if (sender.Amount < request.Amount)
                {
                    throw new ArgumentException("Belopp för stort");
                }

                var result = await _service.Transfer(request);

                if (!result)
                {
                    throw new ArgumentException("Kunde inte slutföra överföring till annan bank");
                }

                sender.Amount -= request.Amount;

                var transaction = new Transaction
                {
                    ReceiverAccount = $"{request.Clearing}-{request.ReceiverAccount}",
                    SenderAccount = request.SenderAccount,
                    Amount = request.Amount,
                    Timestamp = DateTime.Now
                };

                await _context.Transactions.AddAsync(transaction);
                await _context.SaveChangesAsync();

                return Unit.Value;
            }
        }
    }
}