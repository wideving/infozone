using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;

namespace Application.Transfers
{
    public class FromExternalTransfer
    {
        public class Command : IRequest
        {
            [Required]
            public string Clearing { get; set; }
            [Required]
            public string SenderAccount { get; set; }
            [Required]
            public string ReceiverAccount { get; set; }
            [Range(1, 100000000)]
            public int Amount { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DatabaseContext _context;

            public Handler(DatabaseContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {

                var receiver = await _context.Accounts
                    .FirstOrDefaultAsync(a => a.Number == request.ReceiverAccount);

                if (receiver == null)
                {
                    throw new ArgumentException($"Kunde inte hitta ett konto med kontonummer: {request.ReceiverAccount}");
                }

                receiver.Amount += request.Amount;


                var transaction = new Transaction
                {
                    SenderAccount = $"{request.Clearing}-{request.SenderAccount}",
                    ReceiverAccount = request.ReceiverAccount,
                    Amount = request.Amount,
                    Timestamp = DateTime.Now
                };

                await _context.Transactions.AddAsync(transaction);
                await _context.SaveChangesAsync();

                return Unit.Value;
            }
        }
    }
}