using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;

namespace Application.Transfers
{
    public class InternalTransfer
    {
        public class Command : IRequest
        {
            [Required]
            public string ReceiverAccount { get; set; }
            [Required]
            public string SenderAccount { get; set; }
            [Range(1, 100000000)]
            public int Amount { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DatabaseContext _context;
            public Handler(DatabaseContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var sender = await _context.Accounts.FirstOrDefaultAsync(
                    a => a.Number == request.SenderAccount);
                var receiver = await _context.Accounts.FirstOrDefaultAsync(
                    a => a.Number == request.ReceiverAccount);
                var amount = request.Amount;

                if (sender == null)
                {
                    throw new ArgumentException("Det finns ingen avsändare med det kontonumret");
                }

                if (receiver == null)
                {
                    throw new ArgumentException("Det finns ingen mottagare med det kontonumret");
                }

                if (sender.Amount < request.Amount)
                {
                    throw new ArgumentException("Belopp för stort");
                }

                sender.Amount -= request.Amount;
                receiver.Amount += request.Amount;


                var transaction = new Transaction
                {
                    ReceiverAccount = request.ReceiverAccount,
                    SenderAccount = request.SenderAccount,
                    Amount = request.Amount,
                    Timestamp = DateTime.Now
                };

                await _context.Transactions.AddAsync(transaction);
                await _context.SaveChangesAsync();

                return Unit.Value;
            }
        }
    }
}