using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;

namespace Application.Accounts
{
    public class Balance
    {
        public class Query : IRequest<int>
        {
            public string Number { get; set; }
        }

        public class Handler : IRequestHandler<Query, int>
        {
            private readonly DatabaseContext _context;
            public Handler(DatabaseContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(Query request, CancellationToken cancellationToken)
            {
                var account = await _context.Accounts.SingleAsync(a => a.Number == request.Number);
                return account.Amount;
            }
        }
    }
}