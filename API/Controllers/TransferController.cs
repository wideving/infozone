using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Transfers;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransferController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TransferController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("transactions/{accountNumber}")]
        public async Task<ActionResult<IEnumerable<Transaction>>> ListTransactions(string accountNumber)
        {
            try
            {
                var response = await _mediator.Send(new List.Query { Number = accountNumber });
                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                //Log error here
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                //Log error here
                return BadRequest("Hittade inga transaktioner");
            }
        }

        [HttpPost("InternalTransfer")]
        public async Task<ActionResult<Unit>> InternalTransfer(InternalTransfer.Command command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var response = await _mediator.Send(command);
                    return Ok(response);
                }
                catch (ArgumentException ex)
                {
                    //Log error here
                    return BadRequest(ex.Message);
                }
                catch
                {
                    //Log error here
                    return BadRequest("Kunde inte genomföra transaktionen");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpPost("FromExternalTransfer")]
        public async Task<ActionResult<Unit>> FromExternalTransfer(FromExternalTransfer.Command command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var response = await _mediator.Send(command);
                    return Ok(response);
                }
                catch (ArgumentException ex)
                {
                    //Log error here
                    return BadRequest(ex.Message);
                }
                catch
                {
                    //Log error here
                    return BadRequest("Kunde inte genomföra transaktionen");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost("ToExternalTransfer")]
        public async Task<ActionResult<Unit>> ToExternalTransfer(ToExternalTransfer.Command command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var response = await _mediator.Send(command);
                    return Ok(response);
                }
                catch (ArgumentException ex)
                {
                    //Log error
                    return BadRequest(ex.Message);
                }
                catch
                {
                    //Log error
                    return BadRequest("Kunde inte genomföra transaktionen");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }


}