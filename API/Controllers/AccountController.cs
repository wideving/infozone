using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Accounts;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("list-all-accounts")]
        public async Task<ActionResult<IEnumerable<Account>>> ListAllAccounts()
        {
            try
            {
                var accounts = await _mediator.Send(new List.Query());
                return Ok(accounts);
            }
            catch
            {
                //Log error here
                return BadRequest("Kunde inte hämta konton");
            }
        }

        [HttpGet]
        [Route("balance/{accountNumber}")]
        public async Task<ActionResult<int>> Balance(string accountNumber)
        {
            try
            {
                var balance = await _mediator.Send(new Balance.Query { Number = accountNumber });
                return Ok(balance);
            }
            catch
            {
                //Log error here
                return BadRequest($"Kunde inte hämta balansen för konto med kontonummer: {accountNumber}");
            }
        }
    }
}