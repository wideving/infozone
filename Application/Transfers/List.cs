using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;

namespace Application.Transfers
{
    public class List
    {
        public class Query : IRequest<List<Transaction>>
        {
            public string Number { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<Transaction>>
        {
            private readonly DatabaseContext _context;

            public Handler(DatabaseContext context)
            {
                _context = context;
            }

            public async Task<List<Transaction>> Handle(Query request,
                CancellationToken cancellationToken)
            {
                var foundAccount = await _context.Accounts.AnyAsync(a => a.Number == request.Number);
                if (!foundAccount)
                {
                    throw new ArgumentException($"Kunde inte hitta ett konto med kontonummer: {request.Number}");
                }

                var transactions = await _context.Transactions
                    .Where(t => t.ReceiverAccount == request.Number || t.SenderAccount == request.Number)
                    .OrderByDescending(t => t.Timestamp)
                    .ToListAsync();

                return transactions;
            }
        }
    }
}