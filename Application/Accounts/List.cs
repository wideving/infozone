using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;

namespace Application.Accounts
{
    public class List
    {
        public class Query : IRequest<List<Account>> { }

        public class Handler : IRequestHandler<Query, List<Account>>
        {
            private readonly DatabaseContext _context;
            public Handler(DatabaseContext context)
            {
                _context = context;
            }

            public async Task<List<Account>> Handle(Query request,
                CancellationToken cancellationToken)
            {
                return await _context.Accounts.ToListAsync();
            }
        }
    }
}