using Xunit;
using Moq;
using Domain;
using Persistance;
using Application.Transfers;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MockQueryable.Moq;

namespace Application.Tests
{
    public class InternalTransferTest
    {
        [Fact]
        public async Task InternalTransactionDebitCreditIsSame()
        {
            //Arrange
            var mockContext = createDatabaseMock();

            var command = new InternalTransfer.Command();
            command.SenderAccount = "123";
            command.ReceiverAccount = "456";
            command.Amount = 1;

            var expectedSenderAmount = 99;
            var expectedReceiverAmount = 101;

            //Act
            var handler = new InternalTransfer.Handler(mockContext.Object);
            var result = await handler.Handle(command, new CancellationToken());
            var sender = mockContext.Object.Accounts.First(a => a.Number == command.SenderAccount);
            var receiver = mockContext.Object.Accounts.First(a => a.Number == command.ReceiverAccount);

            //Assert
            Assert.Equal(expectedSenderAmount, sender.Amount);
            Assert.Equal(expectedReceiverAmount, receiver.Amount);
        }

        private Mock<DatabaseContext> createDatabaseMock()
        {
            var mockAccountsSet = new List<Account>
            {
                new Account { Number = "123", Amount = 100 },
                new Account { Number = "456", Amount = 100 }
            }.AsQueryable().BuildMockDbSet();

            var mockTransactionsSet = new List<Transaction> { }.AsQueryable().BuildMockDbSet();

            var mockContext = new Mock<DatabaseContext>();
            mockContext.Setup(m => m.Accounts).Returns(mockAccountsSet.Object);
            mockContext.Setup(m => m.Transactions).Returns(mockTransactionsSet.Object);

            return mockContext;
        }
    }

}
