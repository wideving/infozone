using System;

namespace Domain
{
    public class Transaction
    {
        public int Id { get; set; }
        public string ReceiverAccount { get; set; }
        public string SenderAccount { get; set; }
        public int Amount { get; set; }
        public DateTime Timestamp { get; set; }
    }
}