using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Persistance
{
    public class Seed
    {
        public static void SeedData(DatabaseContext context)
        {
            if (!context.Accounts.Any())
            {
                var accounts = new List<Account>
                {
                    new Account
                    {
                        Number = "123",
                        Amount = 1000
                    },
                    new Account
                    {
                        Number = "456",
                        Amount = 1000
                    }
                };

                context.Accounts.AddRange(accounts);
                context.SaveChanges();
            }

        }
    }
}