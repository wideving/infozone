namespace Domain
{
    public class Account
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public int Amount { get; set; }
    }
}