using System.Threading.Tasks;
using Application.Transfers;

namespace Application.Services
{
    public interface IExternalTransferService
    {
        Task<bool> Transfer(ToExternalTransfer.Command command);
    }

    public class ExternalTransferService : IExternalTransferService
    {
        public async Task<bool> Transfer(ToExternalTransfer.Command command)
        {
            await Task.Delay(1000);
            return await Task.FromResult(true);
        }
    }
}